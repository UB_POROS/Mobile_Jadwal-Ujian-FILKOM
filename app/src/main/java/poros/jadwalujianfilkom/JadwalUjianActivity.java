package poros.jadwalujianfilkom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

import poros.jadwalujianfilkom.getjadwal.JadwalJumat;
import poros.jadwalujianfilkom.getjadwal.JadwalKamis;
import poros.jadwalujianfilkom.getjadwal.JadwalRabu;
import poros.jadwalujianfilkom.getjadwal.JadwalSabtu;
import poros.jadwalujianfilkom.getjadwal.JadwalSelasa;
import poros.jadwalujianfilkom.getjadwal.JadwalSenin;
import poros.jadwalujianfilkom.pojo.DetailJadwal;
import poros.jadwalujianfilkom.pojo.jadwalujian.JadwalUjianResponse;
import poros.jadwalujianfilkom.service.Service;
import poros.jadwalujianfilkom.service.generator.FilkomGenerator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JadwalUjianActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup rgJadwal;
    private RadioButton rbJadwal;
    private Button btnJadwal;
    private TextView tvJadwal;

    static private JadwalUjianResponse jadwalUjianResponse;
    private ArrayList<DetailJadwal> jadwals;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal_ujian);


        tvJadwal = (TextView) findViewById(R.id.tv_jadwal);
        rgJadwal = (RadioGroup) findViewById(R.id.rg_jadwal);
        btnJadwal = (Button) findViewById(R.id.btn_lihat_jadwal);
        btnJadwal.setOnClickListener(this);

        getJadwalUjian();

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        jadwals = (ArrayList<DetailJadwal>) args.getSerializable("ARRAYLIST");
    }

    private void getJadwalUjian() {
        Service service = FilkomGenerator.createService(Service.class);

        Call<JadwalUjianResponse> call = service.jadwalUjian();
        call.enqueue(new Callback<JadwalUjianResponse>() {
            @Override
            public void onResponse(Call<JadwalUjianResponse> call, Response<JadwalUjianResponse> response) {
                jadwalUjianResponse = response.body();


                for (int i = 0; i < jadwalUjianResponse.getSenin().getE14().size(); i++) {
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getDosen());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getHari());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getJamMulai());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getJamSelesai());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getJenis());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getKelas());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getProdi());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getRuang());
//                    Log.d("ruangA216", "onResponse: " + jadwalUjianResponse.getRabu().getA216().get(i).getMatakuliah());
                }
            }

            @Override
            public void onFailure(Call<JadwalUjianResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int selectedId = rgJadwal.getCheckedRadioButtonId();

        rbJadwal = (RadioButton) findViewById(selectedId);

        tvJadwal.setText("");

        switch (rbJadwal.getText().toString()) {
            case "semua hari":
                getJadwalUjianSenin();
                getJadwalUjianSelasa();
                getJadwalUjianRabu();
                getJadwalUjianKamis();
                getJadwalUjianJumat();
                getJadwalUjianSabtu();
                break;
            case "senin":
                getJadwalUjianSenin();
                break;
            case "selasa":
                getJadwalUjianSelasa();
                break;
            case "rabu":
                getJadwalUjianRabu();
                break;
            case "kamis":
                getJadwalUjianKamis();
                break;
            case "jumat":
                getJadwalUjianJumat();
                break;
            case "sabtu":
                getJadwalUjianSabtu();
                break;
        }
    }

    private void getJadwalUjianSenin() {
        for (int i = 0; i < jadwals.size(); i++) {
            String matkul = jadwals.get(i).getMatkul();
            String kelas = jadwals.get(i).getKelas();

            String text = JadwalSenin.getSenin(matkul, kelas, jadwalUjianResponse);

            if (!text.equals("")) {
                String updateText = tvJadwal.getText().toString() + text + "\n";
                tvJadwal.setText(updateText);
            }
//            Toast.makeText(JadwalUjianActivity.this, text, Toast.LENGTH_SHORT).show();
        }
    }

    private void getJadwalUjianSelasa() {
        for (int i = 0; i < jadwals.size(); i++) {
            String matkul = jadwals.get(i).getMatkul();
            String kelas = jadwals.get(i).getKelas();

            String text = JadwalSelasa.getSelasa(matkul, kelas, jadwalUjianResponse);

            if (!text.equals("")) {
                String updateText = tvJadwal.getText().toString() + text + "\n";
                tvJadwal.setText(updateText);
            }

//            Toast.makeText(JadwalUjianActivity.this, text, Toast.LENGTH_SHORT).show();
        }
    }

    private void getJadwalUjianRabu() {
        for (int i = 0; i < jadwals.size(); i++) {
            String matkul = jadwals.get(i).getMatkul();
            String kelas = jadwals.get(i).getKelas();

            String text = JadwalRabu.getRabu(matkul, kelas, jadwalUjianResponse);

            if (!text.equals("")) {
                String updateText = tvJadwal.getText().toString() + text + "\n";
                tvJadwal.setText(updateText);
            }
//            Toast.makeText(JadwalUjianActivity.this, text, Toast.LENGTH_SHORT).show();
        }
    }

    private void getJadwalUjianKamis() {
        for (int i = 0; i < jadwals.size(); i++) {
            String matkul = jadwals.get(i).getMatkul();
            String kelas = jadwals.get(i).getKelas();

            String text = JadwalKamis.getKamis(matkul, kelas, jadwalUjianResponse);

            if (!text.equals("")) {
                String updateText = tvJadwal.getText().toString() + text + "\n";
                tvJadwal.setText(updateText);
            }
//            Toast.makeText(JadwalUjianActivity.this, text, Toast.LENGTH_SHORT).show();
        }
    }

    private void getJadwalUjianJumat() {
        for (int i = 0; i < jadwals.size(); i++) {
            String matkul = jadwals.get(i).getMatkul();
            String kelas = jadwals.get(i).getKelas();

            String text = JadwalJumat.getJumat(matkul, kelas, jadwalUjianResponse);

            if (!text.equals("")) {
                String updateText = tvJadwal.getText().toString() + text + "\n";
                tvJadwal.setText(updateText);
            }
//            Toast.makeText(JadwalUjianActivity.this, text, Toast.LENGTH_SHORT).show();
        }
    }

    private void getJadwalUjianSabtu() {
        for (int i = 0; i < jadwals.size(); i++) {
            String matkul = jadwals.get(i).getMatkul();
            String kelas = jadwals.get(i).getKelas();

            String text = JadwalSabtu.getSabtu(matkul, kelas, jadwalUjianResponse);

            if (!text.equals("")) {
                String updateText = tvJadwal.getText().toString() + text + "\n";
                tvJadwal.setText(updateText);
            }
//            Toast.makeText(JadwalUjianActivity.this, text, Toast.LENGTH_SHORT).show();
        }
    }

}
