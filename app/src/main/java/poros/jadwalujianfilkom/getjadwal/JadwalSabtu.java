package poros.jadwalujianfilkom.getjadwal;

import poros.jadwalujianfilkom.pojo.jadwalujian.A216Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A217Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A218Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A219Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A220Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A222Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A223Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A224Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A225Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E11Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E12Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E13Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E14Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E15Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E21Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E22Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E23Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E24Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E25Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E27Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E29Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.JadwalUjianResponse;

/**
 * Created by nyamuk on 12/20/17.
 */

public class JadwalSabtu {

    public static String getSabtu(String matkul, String kelas, JadwalUjianResponse jadwalUjianResponse) {
        String toast = "";

        for (A216Item item : jadwalUjianResponse.getSabtu().getA216()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A217Item item : jadwalUjianResponse.getSabtu().getA217()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A218Item item : jadwalUjianResponse.getSabtu().getA218()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A219Item item : jadwalUjianResponse.getSabtu().getA219()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A220Item item : jadwalUjianResponse.getSabtu().getA220()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A222Item item : jadwalUjianResponse.getSabtu().getA222()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A223Item item : jadwalUjianResponse.getSabtu().getA223()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A224Item item : jadwalUjianResponse.getSabtu().getA224()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A225Item item : jadwalUjianResponse.getSabtu().getA225()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E11Item item : jadwalUjianResponse.getSabtu().getE11()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E12Item item : jadwalUjianResponse.getSabtu().getE12()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E13Item item : jadwalUjianResponse.getSabtu().getE13()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E14Item item : jadwalUjianResponse.getSabtu().getE14()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E15Item item : jadwalUjianResponse.getSabtu().getE15()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (E16Item item : jadwalUjianResponse.getSabtu().getE16()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (E17Item item : jadwalUjianResponse.getSabtu().getE17()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (E18Item item : jadwalUjianResponse.getSabtu().getE18()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }

        for (E21Item item : jadwalUjianResponse.getSabtu().getE21()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E22Item item : jadwalUjianResponse.getSabtu().getE22()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E23Item item : jadwalUjianResponse.getSabtu().getE23()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E24Item item : jadwalUjianResponse.getSabtu().getE24()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E25Item item : jadwalUjianResponse.getSabtu().getE25()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E27Item item : jadwalUjianResponse.getSabtu().getE27()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (E28Item item : jadwalUjianResponse.getSabtu().getE28()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }

        for (E29Item item : jadwalUjianResponse.getSabtu().getE29()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (F21Item item : jadwalUjianResponse.getSabtu().getF21()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F22Item item : jadwalUjianResponse.getSabtu().getF22()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F24Item item : jadwalUjianResponse.getSabtu().getF24()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F25Item item : jadwalUjianResponse.getSabtu().getF25()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F26Item item : jadwalUjianResponse.getSabtu().getF26()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F28Item item : jadwalUjianResponse.getSabtu().getF28()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F29Item item : jadwalUjianResponse.getSabtu().getF29()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F31Item item : jadwalUjianResponse.getSabtu().getF31()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F33Item item : jadwalUjianResponse.getSabtu().getF33()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F34Item item : jadwalUjianResponse.getSabtu().getF34()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F35Item item : jadwalUjianResponse.getSabtu().getF35()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F36Item item : jadwalUjianResponse.getSabtu().getF36()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F37Item item : jadwalUjianResponse.getSabtu().getF37()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F38Item item : jadwalUjianResponse.getSabtu().getF38()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F39Item item : jadwalUjianResponse.getSabtu().getF39()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F310Item item : jadwalUjianResponse.getSabtu().getF310()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F311Item item : jadwalUjianResponse.getSabtu().getF311()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F312Item item : jadwalUjianResponse.getSabtu().getF312()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }




        return toast;
    }
}
