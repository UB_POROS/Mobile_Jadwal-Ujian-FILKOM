package poros.jadwalujianfilkom.getjadwal;

import poros.jadwalujianfilkom.pojo.jadwalujian.A216Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A217Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A218Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A219Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A220Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A222Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A223Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A224Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A225Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E11Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E12Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E13Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E14Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E15Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E21Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E22Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E23Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E24Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E25Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E27Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E28Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E29Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F21Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F22Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F24Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F25Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F26Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F28Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F29Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F310Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F311Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F31Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F33Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F34Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F35Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F36Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F39Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.JadwalUjianResponse;

/**
 * Created by nyamuk on 12/20/17.
 */

public class JadwalSenin {

    public static String getSenin(String matkul, String kelas, JadwalUjianResponse jadwalUjianResponse) {
        String toast = "";

        for (A216Item item : jadwalUjianResponse.getSenin().getA216()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A217Item item : jadwalUjianResponse.getSenin().getA217()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A218Item item : jadwalUjianResponse.getSenin().getA218()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A219Item item : jadwalUjianResponse.getSenin().getA219()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A220Item item : jadwalUjianResponse.getSenin().getA220()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A222Item item : jadwalUjianResponse.getSenin().getA222()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A223Item item : jadwalUjianResponse.getSenin().getA223()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A224Item item : jadwalUjianResponse.getSenin().getA224()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A225Item item : jadwalUjianResponse.getSenin().getA225()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E11Item item : jadwalUjianResponse.getSenin().getE11()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E12Item item : jadwalUjianResponse.getSenin().getE12()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E13Item item : jadwalUjianResponse.getSenin().getE13()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E14Item item : jadwalUjianResponse.getSenin().getE14()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E15Item item : jadwalUjianResponse.getSenin().getE15()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (E16Item item : jadwalUjianResponse.getSenin().getE16()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (E17Item item : jadwalUjianResponse.getSenin().getE17()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (E18Item item : jadwalUjianResponse.getSenin().getE18()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }

        for (E21Item item : jadwalUjianResponse.getSenin().getE21()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E22Item item : jadwalUjianResponse.getSenin().getE22()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E23Item item : jadwalUjianResponse.getSenin().getE23()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E24Item item : jadwalUjianResponse.getSenin().getE24()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E25Item item : jadwalUjianResponse.getSenin().getE25()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E27Item item : jadwalUjianResponse.getSenin().getE27()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E28Item item : jadwalUjianResponse.getSenin().getE28()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E29Item item : jadwalUjianResponse.getSenin().getE29()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F21Item item : jadwalUjianResponse.getSenin().getF21()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F22Item item : jadwalUjianResponse.getSenin().getF22()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F24Item item : jadwalUjianResponse.getSenin().getF24()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F25Item item : jadwalUjianResponse.getSenin().getF25()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F26Item item : jadwalUjianResponse.getSenin().getF26()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F28Item item : jadwalUjianResponse.getSenin().getF28()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F29Item item : jadwalUjianResponse.getSenin().getF29()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F31Item item : jadwalUjianResponse.getSenin().getF31()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F33Item item : jadwalUjianResponse.getSenin().getF33()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F34Item item : jadwalUjianResponse.getSenin().getF34()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F35Item item : jadwalUjianResponse.getSenin().getF35()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F36Item item : jadwalUjianResponse.getSenin().getF36()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (F37Item item : jadwalUjianResponse.getSenin().getF37()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }
//
//        for (F38Item item : jadwalUjianResponse.getSenin().getF38()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }

        for (F39Item item : jadwalUjianResponse.getSenin().getF39()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F310Item item : jadwalUjianResponse.getSenin().getF310()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F311Item item : jadwalUjianResponse.getSenin().getF311()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (F312Item item : jadwalUjianResponse.getSenin().getF312()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }




        return toast;
    }
}
