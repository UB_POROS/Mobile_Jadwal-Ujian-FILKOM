package poros.jadwalujianfilkom.getjadwal;

import poros.jadwalujianfilkom.pojo.jadwalujian.A216Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A217Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A218Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A219Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A220Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A222Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A223Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A224Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.A225Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E11Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E12Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E13Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E14Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E15Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E17Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E18Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E21Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E22Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E23Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E24Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E25Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E27Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E28Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.E29Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F21Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F22Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F24Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F25Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F26Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F28Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F29Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F310Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F311Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F312Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F31Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F33Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F34Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F35Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F36Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F37Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F38Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.F39Item;
import poros.jadwalujianfilkom.pojo.jadwalujian.JadwalUjianResponse;

/**
 * Created by nyamuk on 12/20/17.
 */

public class JadwalJumat {

    public static String getJumat(String matkul, String kelas, JadwalUjianResponse jadwalUjianResponse) {
        String toast = "";

        for (A216Item item : jadwalUjianResponse.getJumat().getA216()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A217Item item : jadwalUjianResponse.getJumat().getA217()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A218Item item : jadwalUjianResponse.getJumat().getA218()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A219Item item : jadwalUjianResponse.getJumat().getA219()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A220Item item : jadwalUjianResponse.getJumat().getA220()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A222Item item : jadwalUjianResponse.getJumat().getA222()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A223Item item : jadwalUjianResponse.getJumat().getA223()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A224Item item : jadwalUjianResponse.getJumat().getA224()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (A225Item item : jadwalUjianResponse.getJumat().getA225()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E11Item item : jadwalUjianResponse.getJumat().getE11()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E12Item item : jadwalUjianResponse.getJumat().getE12()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E13Item item : jadwalUjianResponse.getJumat().getE13()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E14Item item : jadwalUjianResponse.getJumat().getE14()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E15Item item : jadwalUjianResponse.getJumat().getE15()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

//        for (E16Item item : jadwalUjianResponse.getJumat().getE16()) {
//            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
//                    kelas.trim().equals(item.getKelas().trim())) {
//                toast = toast + item.getMatakuliah() + " ";
//                toast = toast + item.getJamMulai() + " ";
//                toast = toast + item.getJamSelesai() + " ";
//                toast = toast + item.getRuang() + " ";
//                toast = toast + item.getKelas() + " ";
//                toast = toast + item.getTgl() + " ";
//            }
//        }

        for (E17Item item : jadwalUjianResponse.getJumat().getE17()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E18Item item : jadwalUjianResponse.getJumat().getE18()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E21Item item : jadwalUjianResponse.getJumat().getE21()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E22Item item : jadwalUjianResponse.getJumat().getE22()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E23Item item : jadwalUjianResponse.getJumat().getE23()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E24Item item : jadwalUjianResponse.getJumat().getE24()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E25Item item : jadwalUjianResponse.getJumat().getE25()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E27Item item : jadwalUjianResponse.getJumat().getE27()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E28Item item : jadwalUjianResponse.getJumat().getE28()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (E29Item item : jadwalUjianResponse.getJumat().getE29()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F21Item item : jadwalUjianResponse.getJumat().getF21()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F22Item item : jadwalUjianResponse.getJumat().getF22()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F24Item item : jadwalUjianResponse.getJumat().getF24()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F25Item item : jadwalUjianResponse.getJumat().getF25()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F26Item item : jadwalUjianResponse.getJumat().getF26()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F28Item item : jadwalUjianResponse.getJumat().getF28()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F29Item item : jadwalUjianResponse.getJumat().getF29()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F31Item item : jadwalUjianResponse.getJumat().getF31()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F33Item item : jadwalUjianResponse.getJumat().getF33()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F34Item item : jadwalUjianResponse.getJumat().getF34()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F35Item item : jadwalUjianResponse.getJumat().getF35()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F36Item item : jadwalUjianResponse.getJumat().getF36()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F37Item item : jadwalUjianResponse.getJumat().getF37()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F38Item item : jadwalUjianResponse.getJumat().getF38()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F39Item item : jadwalUjianResponse.getJumat().getF39()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F310Item item : jadwalUjianResponse.getJumat().getF310()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F311Item item : jadwalUjianResponse.getJumat().getF311()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }

        for (F312Item item : jadwalUjianResponse.getJumat().getF312()) {
            if (matkul.trim().equals(item.getMatakuliah().trim()) &&
                    kelas.trim().equals(item.getKelas().trim())) {
                toast = toast + item.getMatakuliah() + " ";
                toast = toast + item.getJamMulai() + " ";
                toast = toast + item.getJamSelesai() + " ";
                toast = toast + item.getRuang() + " ";
                toast = toast + item.getKelas() + " ";
                toast = toast + item.getTgl() + " ";
            }
        }




        return toast;
    }
}
