package poros.jadwalujianfilkom.pojo.jadwalujian;

import com.google.gson.annotations.SerializedName;

public class E18Item{

	@SerializedName("hari")
	private String hari;

	@SerializedName("jam_mulai")
	private String jamMulai;

	@SerializedName("dosen")
	private String dosen;

	@SerializedName("jam_selesai")
	private String jamSelesai;

	@SerializedName("ruang")
	private String ruang;

	@SerializedName("urut")
	private String urut;

	@SerializedName("prodi")
	private String prodi;

	@SerializedName("mk_id")
	private String mkId;

	@SerializedName("kelas")
	private String kelas;

	@SerializedName("tgl")
	private String tgl;

	@SerializedName("jenis")
	private String jenis;

	@SerializedName("lang")
	private String lang;

	@SerializedName("matakuliah")
	private String matakuliah;

	public void setHari(String hari){
		this.hari = hari;
	}

	public String getHari(){
		return hari;
	}

	public void setJamMulai(String jamMulai){
		this.jamMulai = jamMulai;
	}

	public String getJamMulai(){
		return jamMulai;
	}

	public void setDosen(String dosen){
		this.dosen = dosen;
	}

	public String getDosen(){
		return dosen;
	}

	public void setJamSelesai(String jamSelesai){
		this.jamSelesai = jamSelesai;
	}

	public String getJamSelesai(){
		return jamSelesai;
	}

	public void setRuang(String ruang){
		this.ruang = ruang;
	}

	public String getRuang(){
		return ruang;
	}

	public void setUrut(String urut){
		this.urut = urut;
	}

	public String getUrut(){
		return urut;
	}

	public void setProdi(String prodi){
		this.prodi = prodi;
	}

	public String getProdi(){
		return prodi;
	}

	public void setMkId(String mkId){
		this.mkId = mkId;
	}

	public String getMkId(){
		return mkId;
	}

	public void setKelas(String kelas){
		this.kelas = kelas;
	}

	public String getKelas(){
		return kelas;
	}

	public void setTgl(String tgl){
		this.tgl = tgl;
	}

	public String getTgl(){
		return tgl;
	}

	public void setJenis(String jenis){
		this.jenis = jenis;
	}

	public String getJenis(){
		return jenis;
	}

	public void setLang(String lang){
		this.lang = lang;
	}

	public String getLang(){
		return lang;
	}

	public void setMatakuliah(String matakuliah){
		this.matakuliah = matakuliah;
	}

	public String getMatakuliah(){
		return matakuliah;
	}

	@Override
 	public String toString(){
		return 
			"E18Item{" + 
			"hari = '" + hari + '\'' + 
			",jam_mulai = '" + jamMulai + '\'' + 
			",dosen = '" + dosen + '\'' + 
			",jam_selesai = '" + jamSelesai + '\'' + 
			",ruang = '" + ruang + '\'' + 
			",urut = '" + urut + '\'' + 
			",prodi = '" + prodi + '\'' + 
			",mk_id = '" + mkId + '\'' + 
			",kelas = '" + kelas + '\'' + 
			",tgl = '" + tgl + '\'' + 
			",jenis = '" + jenis + '\'' + 
			",lang = '" + lang + '\'' + 
			",matakuliah = '" + matakuliah + '\'' + 
			"}";
		}
}