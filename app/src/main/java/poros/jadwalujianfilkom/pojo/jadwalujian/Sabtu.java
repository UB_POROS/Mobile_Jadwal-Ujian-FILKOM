package poros.jadwalujianfilkom.pojo.jadwalujian;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Sabtu{

	@SerializedName("A2.17")
	private List<A217Item> a217;

	@SerializedName("A2.16")
	private List<A216Item> a216;

	@SerializedName("A2.25")
	private List<A225Item> a225;

	@SerializedName("A2.19")
	private List<A219Item> a219;

	@SerializedName("A2.18")
	private List<A218Item> a218;

	@SerializedName("E1.1")
	private List<E11Item> e11;

	@SerializedName("A2.20")
	private List<A220Item> a220;

	@SerializedName("E1.2")
	private List<E12Item> e12;

	@SerializedName("E2.1")
	private List<E21Item> e21;

	@SerializedName("E1.3")
	private List<E13Item> e13;

	@SerializedName("E2.2")
	private List<E22Item> e22;

	@SerializedName("E1.4")
	private List<E14Item> e14;

	@SerializedName("E2.3")
	private List<E23Item> e23;

	@SerializedName("E1.5")
	private List<E15Item> e15;

	@SerializedName("E2.4")
	private List<E24Item> e24;

	@SerializedName("A2.24")
	private List<A224Item> a224;

	@SerializedName("E2.5")
	private List<E25Item> e25;

	@SerializedName("A2.23")
	private List<A223Item> a223;

	@SerializedName("A2.22")
	private List<A222Item> a222;

	@SerializedName("E2.7")
	private List<E27Item> e27;

	@SerializedName("E2.9")
	private List<E29Item> e29;

	public void setA217(List<A217Item> a217){
		this.a217 = a217;
	}

	public List<A217Item> getA217(){
		return a217;
	}

	public void setA216(List<A216Item> a216){
		this.a216 = a216;
	}

	public List<A216Item> getA216(){
		return a216;
	}

	public void setA225(List<A225Item> a225){
		this.a225 = a225;
	}

	public List<A225Item> getA225(){
		return a225;
	}

	public void setA219(List<A219Item> a219){
		this.a219 = a219;
	}

	public List<A219Item> getA219(){
		return a219;
	}

	public void setA218(List<A218Item> a218){
		this.a218 = a218;
	}

	public List<A218Item> getA218(){
		return a218;
	}

	public void setE11(List<E11Item> e11){
		this.e11 = e11;
	}

	public List<E11Item> getE11(){
		return e11;
	}

	public void setA220(List<A220Item> a220){
		this.a220 = a220;
	}

	public List<A220Item> getA220(){
		return a220;
	}

	public void setE12(List<E12Item> e12){
		this.e12 = e12;
	}

	public List<E12Item> getE12(){
		return e12;
	}

	public void setE21(List<E21Item> e21){
		this.e21 = e21;
	}

	public List<E21Item> getE21(){
		return e21;
	}

	public void setE13(List<E13Item> e13){
		this.e13 = e13;
	}

	public List<E13Item> getE13(){
		return e13;
	}

	public void setE22(List<E22Item> e22){
		this.e22 = e22;
	}

	public List<E22Item> getE22(){
		return e22;
	}

	public void setE14(List<E14Item> e14){
		this.e14 = e14;
	}

	public List<E14Item> getE14(){
		return e14;
	}

	public void setE23(List<E23Item> e23){
		this.e23 = e23;
	}

	public List<E23Item> getE23(){
		return e23;
	}

	public void setE15(List<E15Item> e15){
		this.e15 = e15;
	}

	public List<E15Item> getE15(){
		return e15;
	}

	public void setE24(List<E24Item> e24){
		this.e24 = e24;
	}

	public List<E24Item> getE24(){
		return e24;
	}

	public void setA224(List<A224Item> a224){
		this.a224 = a224;
	}

	public List<A224Item> getA224(){
		return a224;
	}

	public void setE25(List<E25Item> e25){
		this.e25 = e25;
	}

	public List<E25Item> getE25(){
		return e25;
	}

	public void setA223(List<A223Item> a223){
		this.a223 = a223;
	}

	public List<A223Item> getA223(){
		return a223;
	}

	public void setA222(List<A222Item> a222){
		this.a222 = a222;
	}

	public List<A222Item> getA222(){
		return a222;
	}

	public void setE27(List<E27Item> e27){
		this.e27 = e27;
	}

	public List<E27Item> getE27(){
		return e27;
	}

	public void setE29(List<E29Item> e29){
		this.e29 = e29;
	}

	public List<E29Item> getE29(){
		return e29;
	}

	@Override
 	public String toString(){
		return 
			"Sabtu{" + 
			"a2.17 = '" + a217 + '\'' + 
			",a2.16 = '" + a216 + '\'' + 
			",a2.25 = '" + a225 + '\'' + 
			",a2.19 = '" + a219 + '\'' + 
			",a2.18 = '" + a218 + '\'' + 
			",e1.1 = '" + e11 + '\'' + 
			",a2.20 = '" + a220 + '\'' + 
			",e1.2 = '" + e12 + '\'' + 
			",e2.1 = '" + e21 + '\'' + 
			",e1.3 = '" + e13 + '\'' + 
			",e2.2 = '" + e22 + '\'' + 
			",e1.4 = '" + e14 + '\'' + 
			",e2.3 = '" + e23 + '\'' + 
			",e1.5 = '" + e15 + '\'' + 
			",e2.4 = '" + e24 + '\'' + 
			",a2.24 = '" + a224 + '\'' + 
			",e2.5 = '" + e25 + '\'' + 
			",a2.23 = '" + a223 + '\'' + 
			",a2.22 = '" + a222 + '\'' + 
			",e2.7 = '" + e27 + '\'' + 
			",e2.9 = '" + e29 + '\'' + 
			"}";
		}
}