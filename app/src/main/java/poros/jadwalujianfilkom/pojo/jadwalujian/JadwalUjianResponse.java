package poros.jadwalujianfilkom.pojo.jadwalujian;

import com.google.gson.annotations.SerializedName;

public class JadwalUjianResponse{

	@SerializedName("kamis")
	private Kamis kamis;

	@SerializedName("senin")
	private Senin senin;

	@SerializedName("rabu")
	private Rabu rabu;

	@SerializedName("sabtu")
	private Sabtu sabtu;

	@SerializedName("jumat")
	private Jumat jumat;

	@SerializedName("selasa")
	private Selasa selasa;

	public void setKamis(Kamis kamis){
		this.kamis = kamis;
	}

	public Kamis getKamis(){
		return kamis;
	}

	public void setSenin(Senin senin){
		this.senin = senin;
	}

	public Senin getSenin(){
		return senin;
	}

	public void setRabu(Rabu rabu){
		this.rabu = rabu;
	}

	public Rabu getRabu(){
		return rabu;
	}

	public void setSabtu(Sabtu sabtu){
		this.sabtu = sabtu;
	}

	public Sabtu getSabtu(){
		return sabtu;
	}

	public void setJumat(Jumat jumat){
		this.jumat = jumat;
	}

	public Jumat getJumat(){
		return jumat;
	}

	public void setSelasa(Selasa selasa){
		this.selasa = selasa;
	}

	public Selasa getSelasa(){
		return selasa;
	}

	@Override
 	public String toString(){
		return 
			"JadwalUjianResponse{" + 
			"kamis = '" + kamis + '\'' + 
			",senin = '" + senin + '\'' + 
			",rabu = '" + rabu + '\'' + 
			",sabtu = '" + sabtu + '\'' + 
			",jumat = '" + jumat + '\'' + 
			",selasa = '" + selasa + '\'' + 
			"}";
		}
}