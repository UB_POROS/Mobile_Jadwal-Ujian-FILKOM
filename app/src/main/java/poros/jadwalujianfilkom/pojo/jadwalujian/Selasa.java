package poros.jadwalujianfilkom.pojo.jadwalujian;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Selasa{

	@SerializedName("F2.1")
	private List<F21Item> f21;

	@SerializedName("F2.2")
	private List<F22Item> f22;

	@SerializedName("F3.1")
	private List<F31Item> f31;

	@SerializedName("F2.4")
	private List<F24Item> f24;

	@SerializedName("F3.3")
	private List<F33Item> f33;

	@SerializedName("F2.5")
	private List<F25Item> f25;

	@SerializedName("F3.4")
	private List<F34Item> f34;

	@SerializedName("F2.6")
	private List<F26Item> f26;

	@SerializedName("F3.5")
	private List<F35Item> f35;

	@SerializedName("F2.8")
	private List<F28Item> f28;

	@SerializedName("F3.7")
	private List<F37Item> f37;

	@SerializedName("F2.9")
	private List<F29Item> f29;

	@SerializedName("F3.8")
	private List<F38Item> f38;

	@SerializedName("F3.9")
	private List<F39Item> f39;

	@SerializedName("E1.1")
	private List<E11Item> e11;

	@SerializedName("E1.2")
	private List<E12Item> e12;

	@SerializedName("E2.1")
	private List<E21Item> e21;

	@SerializedName("F3.12")
	private List<F312Item> f312;

	@SerializedName("E1.3")
	private List<E13Item> e13;

	@SerializedName("E2.2")
	private List<E22Item> e22;

	@SerializedName("E1.4")
	private List<E14Item> e14;

	@SerializedName("E2.3")
	private List<E23Item> e23;

	@SerializedName("F3.10")
	private List<F310Item> f310;

	@SerializedName("E1.5")
	private List<E15Item> e15;

	@SerializedName("E2.4")
	private List<E24Item> e24;

	@SerializedName("F3.11")
	private List<F311Item> f311;

	@SerializedName("E2.5")
	private List<E25Item> e25;

	@SerializedName("E2.7")
	private List<E27Item> e27;

	@SerializedName("E2.8")
	private List<E28Item> e28;

	@SerializedName("E2.9")
	private List<E29Item> e29;

	public void setF21(List<F21Item> f21){
		this.f21 = f21;
	}

	public List<F21Item> getF21(){
		return f21;
	}

	public void setF22(List<F22Item> f22){
		this.f22 = f22;
	}

	public List<F22Item> getF22(){
		return f22;
	}

	public void setF31(List<F31Item> f31){
		this.f31 = f31;
	}

	public List<F31Item> getF31(){
		return f31;
	}

	public void setF24(List<F24Item> f24){
		this.f24 = f24;
	}

	public List<F24Item> getF24(){
		return f24;
	}

	public void setF33(List<F33Item> f33){
		this.f33 = f33;
	}

	public List<F33Item> getF33(){
		return f33;
	}

	public void setF25(List<F25Item> f25){
		this.f25 = f25;
	}

	public List<F25Item> getF25(){
		return f25;
	}

	public void setF34(List<F34Item> f34){
		this.f34 = f34;
	}

	public List<F34Item> getF34(){
		return f34;
	}

	public void setF26(List<F26Item> f26){
		this.f26 = f26;
	}

	public List<F26Item> getF26(){
		return f26;
	}

	public void setF35(List<F35Item> f35){
		this.f35 = f35;
	}

	public List<F35Item> getF35(){
		return f35;
	}

	public void setF28(List<F28Item> f28){
		this.f28 = f28;
	}

	public List<F28Item> getF28(){
		return f28;
	}

	public void setF37(List<F37Item> f37){
		this.f37 = f37;
	}

	public List<F37Item> getF37(){
		return f37;
	}

	public void setF29(List<F29Item> f29){
		this.f29 = f29;
	}

	public List<F29Item> getF29(){
		return f29;
	}

	public void setF38(List<F38Item> f38){
		this.f38 = f38;
	}

	public List<F38Item> getF38(){
		return f38;
	}

	public void setF39(List<F39Item> f39){
		this.f39 = f39;
	}

	public List<F39Item> getF39(){
		return f39;
	}

	public void setE11(List<E11Item> e11){
		this.e11 = e11;
	}

	public List<E11Item> getE11(){
		return e11;
	}

	public void setE12(List<E12Item> e12){
		this.e12 = e12;
	}

	public List<E12Item> getE12(){
		return e12;
	}

	public void setE21(List<E21Item> e21){
		this.e21 = e21;
	}

	public List<E21Item> getE21(){
		return e21;
	}

	public void setF312(List<F312Item> f312){
		this.f312 = f312;
	}

	public List<F312Item> getF312(){
		return f312;
	}

	public void setE13(List<E13Item> e13){
		this.e13 = e13;
	}

	public List<E13Item> getE13(){
		return e13;
	}

	public void setE22(List<E22Item> e22){
		this.e22 = e22;
	}

	public List<E22Item> getE22(){
		return e22;
	}

	public void setE14(List<E14Item> e14){
		this.e14 = e14;
	}

	public List<E14Item> getE14(){
		return e14;
	}

	public void setE23(List<E23Item> e23){
		this.e23 = e23;
	}

	public List<E23Item> getE23(){
		return e23;
	}

	public void setF310(List<F310Item> f310){
		this.f310 = f310;
	}

	public List<F310Item> getF310(){
		return f310;
	}

	public void setE15(List<E15Item> e15){
		this.e15 = e15;
	}

	public List<E15Item> getE15(){
		return e15;
	}

	public void setE24(List<E24Item> e24){
		this.e24 = e24;
	}

	public List<E24Item> getE24(){
		return e24;
	}

	public void setF311(List<F311Item> f311){
		this.f311 = f311;
	}

	public List<F311Item> getF311(){
		return f311;
	}

	public void setE25(List<E25Item> e25){
		this.e25 = e25;
	}

	public List<E25Item> getE25(){
		return e25;
	}

	public void setE27(List<E27Item> e27){
		this.e27 = e27;
	}

	public List<E27Item> getE27(){
		return e27;
	}

	public void setE28(List<E28Item> e28){
		this.e28 = e28;
	}

	public List<E28Item> getE28(){
		return e28;
	}

	public void setE29(List<E29Item> e29){
		this.e29 = e29;
	}

	public List<E29Item> getE29(){
		return e29;
	}

	@Override
 	public String toString(){
		return 
			"Selasa{" + 
			"f2.1 = '" + f21 + '\'' + 
			",f2.2 = '" + f22 + '\'' + 
			",f3.1 = '" + f31 + '\'' + 
			",f2.4 = '" + f24 + '\'' + 
			",f3.3 = '" + f33 + '\'' + 
			",f2.5 = '" + f25 + '\'' + 
			",f3.4 = '" + f34 + '\'' + 
			",f2.6 = '" + f26 + '\'' + 
			",f3.5 = '" + f35 + '\'' + 
			",f2.8 = '" + f28 + '\'' + 
			",f3.7 = '" + f37 + '\'' + 
			",f2.9 = '" + f29 + '\'' + 
			",f3.8 = '" + f38 + '\'' + 
			",f3.9 = '" + f39 + '\'' + 
			",e1.1 = '" + e11 + '\'' + 
			",e1.2 = '" + e12 + '\'' + 
			",e2.1 = '" + e21 + '\'' + 
			",f3.12 = '" + f312 + '\'' + 
			",e1.3 = '" + e13 + '\'' + 
			",e2.2 = '" + e22 + '\'' + 
			",e1.4 = '" + e14 + '\'' + 
			",e2.3 = '" + e23 + '\'' + 
			",f3.10 = '" + f310 + '\'' + 
			",e1.5 = '" + e15 + '\'' + 
			",e2.4 = '" + e24 + '\'' + 
			",f3.11 = '" + f311 + '\'' + 
			",e2.5 = '" + e25 + '\'' + 
			",e2.7 = '" + e27 + '\'' + 
			",e2.8 = '" + e28 + '\'' + 
			",e2.9 = '" + e29 + '\'' + 
			"}";
		}
}